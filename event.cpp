#include "event.h"

Event::Event(){

}

std::string Event::getName(){
  return this->name;
}

std::string Event::getType(){
  return this->type;
}

int Event::getMaxCapacity(){
 return  this->maxCapacity;
}

int Event::getBookedSpace(){
  return this->bookedSpace;
}

Music::Music(std::string name, int amount) : Event(name, "Concert", amount, 0){ 
}

std::vector<std::string> Music::description(){
  std::vector<std::string> list;
  list.push_back(super::getName());
  list.push_back(super::getType());
  list.push_back(std::to_string(super::getBookedSpace()));
  list.push_back(std::to_string(super::getMaxCapacity()));
  return list;
}


Film::Film(std::string name, int amount, bool type) : Event(name,"Movie", amount, 0){
  this->is3D = type;
}

std::vector<std::string> Film::description(){
  std::vector<std::string> list;
  list.push_back(super::getName());
  list.push_back(super::getType());
  list.push_back(std::to_string(super::getBookedSpace()));
  list.push_back(std::to_string(super::getMaxCapacity()));
  list.push_back(std::to_string(this->is3D));
  return list;
}

StandUp::StandUp(std::string name, int amount) : Event(name, "Comedy", amount, 0){
  for(int i = 0; i < 200; i++){
    this->space.push_back(0);
  }
}

  
std::vector<std::string> StandUp::description(){
  std::vector<std::string> list;
  list.push_back(super::getName());
  list.push_back(super::getType());
  list.push_back(std::to_string(super::getBookedSpace()));
  list.push_back(std::to_string(super::getMaxCapacity()));
  for (int i = 0; i < 200; i++){
    list.push_back(std::to_string(this->space[i]));
  }
  return list;
}
