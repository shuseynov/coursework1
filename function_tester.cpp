#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "functions.h"
#include <iostream>
#include <fstream>
#include <vector>

TEST_CASE("Check if inserted string paramater is number or letter", "[check_number]"){

  REQUIRE(check_number("5") == true);
  REQUIRE(check_number("-1") == true);
  REQUIRE(check_number("abc") == false);

}

TEST_CASE("Check if new elements are added to a vector or not" "[read_file]"){

  std::vector<std::string> list;
  std::vector<std::string> empty_list;
  read_file(list);
  REQUIRE(list != empty_list);
  
}

TEST_CASE("3rd element of a vector can be changed(incremented) or not", "[add_book]"){

  std::vector<std::string> list = {"classic", "Concert", "0", "300"};
  std::vector<std::string> expected_list =  {"classic", "Concert", "5", "300"};

  add_book(list, "classic", "5");
  REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));

  SECTION("inserting different argument to 3rd parameter of the function"){

    add_book(list, "classic", "-1");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));

    add_book(list, "classic", "a");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));
  }

  SECTION("inserting different argument to 2nd parameter of the function"){
    
    add_book(list, "abc", "7");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));
  }
}

TEST_CASE("3rd element of a vector can be changed(decremented) or not", "[cancel_book]"){

  std::vector<std::string> list = {"pop", "Concert", "30", "300"};
  std::vector<std::string> expected_list =  {"pop", "Concert", "20", "300"};

  cancel_book(list, "pop", "10");
  REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));

  SECTION("inserting different argument to 3rd parameter of the function"){

    cancel_book(list, "pop", "-1");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));

    cancel_book(list, "pop", "a");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));
  }

  SECTION("inserting different argument to 2nd parameter of the function"){
    
    cancel_book(list, "abc", "7");
    REQUIRE_THAT(list, Catch::Matchers::Equals(expected_list));
  }
}

