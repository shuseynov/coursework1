#ifndef _EVENT_H_
#define _EVENT_H_

#include<string>
#include<vector>


class Event{
 private:
  std::string name;
  std::string type;
  int maxCapacity;
  int bookedSpace;

 public:
  Event();
  Event(std::string name, std::string type, int max, int space) : name(name), type(type), maxCapacity(max), bookedSpace(space) {}
  std::string getName();
  std::string getType();
  int getMaxCapacity();
  int getBookedSpace();
  virtual std::vector<std::string> description() = 0;
};

class Music : public Event{

 public:
  typedef Event super;
  Music(std::string name, int maxCapacity);
  std::vector<std::string> description();
};

class Film : public Event{

private:
  bool is3D;

public:
  typedef Event super;
  Film(std::string name, int maxCapacity, bool is3D);
  std::vector<std::string> description();
};

class StandUp : public Event{

private:
  std::vector<int>space;
  
public:
  typedef Event super;
  StandUp(std::string name, int maxCapacity);
  std::vector<std::string> description();
};


#endif
  
