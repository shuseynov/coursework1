#include "event.h"
#include "functions.h"
#include <iostream>
#include <fstream>
#include <vector>


int main(){
  //Create a vector of Event and add 1 object of each class into it.
  std::vector<Event*>list_events;        
  list_events.push_back(new Music ("classic", 300));
  list_events.push_back(new Film ("holidays", 200, false));
  list_events.push_back(new StandUp ("flash", 200));
  list_events.push_back(new Film ("batman", 200, true));

  //Save all data fields of each object into external file.
  std::vector<std::string> external_data;
  for(Event* event : list_events){
    for(std::string str : event->description()){
      external_data.push_back(str);
    }
  }
  write_file(external_data);

  //Create an empty data base and some variables for switch case.
  std::vector<std::string> data_base;
  bool quit = false;
  std::string choice;
  std::string name;
  std::string amount;
  std::string save;
  
  while(!quit){
    print_options();
    std::cin>>choice;
    
    if(check_number(choice)){
      
      switch(std::stoi(choice)){
		  
      case 1:
	std::cout<<"Please enter event name\n";
	std::cin>>name;
	std::cout<<"How many seat would you like to book?\n";
	std::cin>>amount;
	add_book(data_base,name,amount);
	break;
	  
      case 2:
	std::cout<<"Please enter event name\n";
	std::cin>>name;
	std::cout<<"How many seat would you like to cancel?\n";
	std::cin>>amount;
	cancel_book(data_base,name,amount);
	break;
	
      case 3:
	print_list(data_base);
	break;
	
      case 4:
	std::cout<<"Please enter event name\n";
	std::cin>>name;
	print_details(data_base,name);
	break;
	
      case 5:
	read_file(data_base);
	std::cout<<"Successfully loaded files\n";
	break;
	
      case 6:
	write_file(data_base);
	std::cout<<"Successfully saved all files\n";
	break;
	
      case 7:
	std::cout<<"Would you like to save files?\n1. Yes\n2. No\n";
	  std::cin>>save;
	  if(check_number(save)){
	    if(std::stoi(save) > 0 && std::stoi(save) < 3){
	      if(std::stoi(save) == 1){
		write_file(data_base);
		std::cout<<"Successfully saved all files\n";
		std::cout<<"Successfull exit\n";
		quit = true;
	      } else{
		std::cout<<"Successfull exit\n";
		quit = true;
	      }	      
	    }else{
	      std::cout<<"Please enter number (1 - 2) next time\nQuit failed\n"; 
	    }	    
	  }else{
	    std::cout<<"Please enter only number to save files next time\nQuit failed\n";
	  }
	  break;
	  
      default:
	std::cout<<"Please enter number (1 - 7)\n";
	break;
      }
    } else {
      std::cout<<"Please enter only numbers to select option"<<std::endl;
    }
  }
  
  return 0; 
  
}


