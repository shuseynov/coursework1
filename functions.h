#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include <iostream>
#include <fstream>
#include <vector>

void print_options();
void clear_file();
bool check_number(std::string str);
void read_file(std::vector<std::string> &ev);
void write_file(std::vector<std::string> ev);
bool check_event(std::vector<std::string> ev, std::string name);
void add_book(std::vector<std::string> &ev, std::string name, std::string amount);
void cancel_book(std::vector<std::string> &ev, std::string name, std::string amount);
void print_list(std::vector<std::string> ev);
void print_details(std::vector<std::string> ev,std::string name);



#endif
