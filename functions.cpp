#include "functions.h"
#include "event.h"

//Print 7 options to user
void print_options(){
  std::cout<<"1. Add book for an event\n2. Cancel book\n3. Show list all events\n4. Show details of an event\n5. Load file\n6. Save file\n7. Quit\nPlease enter a number:"<<std::endl;
}

//Clear external file
void clear_file(){  
  std:: ofstream file("listEvents.txt");
  file<<"";
  file.close();
}

//Check entered string is number or not
bool check_number(std::string str){
  int size = str.length();
  for (int i = 0; i < size; i++) {
    if(std::isdigit(str[i]) == true){
      return true;
    }   
  }
  return false;
}

//Read external file and save it into vector<string>.
void read_file(std::vector<std::string> &ev){
  std::vector<std::string> list;
  std::ifstream read_file ("listEvents.txt");
  std::string output;
  if(read_file.is_open()){
    while(!read_file.eof()){
      read_file >> output;
      list.push_back(output);
    }
  }
  list.pop_back(); 
  read_file.close();
  ev = list;
}

//Clear extrenal file and save new data from vector<string>.
void write_file(std::vector<std::string> ev){
  std::ofstream list_file;
  list_file.open ("listEvents.txt", std::ios::out | std::ios::app);
  while(list_file.is_open()){
    clear_file();
    for(std::string str : ev){
      list_file <<str<<" ";
    }
    list_file.close();
  }
}

//Check if name contains in vector<string>.
bool check_event(std::vector<std::string> ev,std::string name){
  std::vector<std::string> list = ev;
  int size = list.size();
  for (int i = 1; i < size; i+=4){
    if (list[i - 1] == name){
      return true;
    }
    if (list[i] == "Movie"){
      i++;
    }
    if (list[i] == "Comedy"){
      i+=200;
    }
  }
  return false;
}

//Changes specific element of a vector<string>.
void cancel_book(std::vector<std::string> &ev, std::string name, std::string amount){
  int size = ev.size();
  int seat1;
  std::string seat;
  if(check_number(amount)){
    if(std::stoi(amount) > 0){
      if(check_event(ev, name)){
	for (int i = 1; i < size; i+=4){
	  if ((ev[i - 1] == name) && ((ev[i] == "Concert") || (ev[i] == "Movie"))){
	    if ((std::stoi(ev[i + 1]) - std::stoi(amount)) >=0){
	      ev[i + 1] = std::to_string(std::stoi(ev[i + 1]) - std::stoi(amount));
	      std::cout<<"Successfully canceled: "<<amount<<" spaces in: "<<name<<" event\n";
	    } else {
	      std::cout<<"Cannot cancel spaces: "<<amount<<" for event: "<<name<<" Total booked space: "<<ev[i + 1]<<"\nPlease enter less amount next time\n"; 
	    }
	  }
	  if ((ev[i - 1] == name) && (ev[i] == "Comedy")){
	    if ((std::stoi(ev[i + 1]) - std::stoi(amount)) >=0){
	      ev[i + 1] = std::to_string(std::stoi(ev[i + 1]) - std::stoi(amount));   
	      for (int j = 0; j < std::stoi(amount); j+=0){
		std::cout<<"Please enter the seat number:\n";
		std::cin>>seat;
	      if(check_number(seat)){
		seat1 = std::stoi(seat);
		if(seat1 > 0 && seat1 <= 200){
		  if(ev[i + 2 + seat1] != "0"){
		    ev[i + 2 + seat1] = "0";
		    j++;
		  } else {
		    std::cout<<"The seat number: "<<seat<<" is not booked try other seat\n";
		  }
		} else {
		  std::cout<<"Please enter a number (1 - 200)\n";
		}
	      } else {
		std::cout<<"Please enter only numbers\n";
	      }
	      }
	      std::cout<<"Successfully canceled: "<<amount<<" spaces in "<<name<<" event\n";
	    } else {
	      std::cout<<"Cannot cancel: "<<amount<<" for event "<<name<<" Total booked space: "<<ev[i + 1]<<"\nPlease enter less amount\n"; 
	    }
	  }
	  if (ev[i] == "Movie"){
	    i++;
	  }
	  if (ev[i] == "Comedy"){
	  i+=200;
	  }
	}
      }else{
	std::cout<<"Event: "<<name<<" is not exist! Please enter a valid name next time\n";
      }
    } else {
      std::cout<<"Please enter positive number for amount next time\n";
    }
  } else {
    std::cout<<"Please enter only numbers for amount next time\n";
  }
}

//Print specific(name of events) elements of a vector<string>
void print_list(std::vector<std::string> lis){
  int size = lis.size();
  std::cout<<"Events:"<<std::endl;
  for(int i = 1; i < size; i+=4){
    
    std::cout<<lis[i - 1]<<std::endl;
    
    if (lis[i] == "Movie"){
      i++;
    }
    
    if (lis[i] == "Comedy"){
      i+=200;
      }
  }
}

//Print specific(event details) elements of a vector<string>
void print_details(std::vector<std::string> lis, std::string name){
  int size = lis.size();
  if(check_event(lis, name)){
    std::cout<<"Events details:"<<std::endl;
    for(int i = 1; i < size; i+=4){
      
      if(lis[i -1] == name && lis[i] == "Concert"){
	std::cout<<lis[i - 1]<<"  "<<lis[i]<<"  "<<lis[i + 1]<<"  "<<lis[i + 2]<<std::endl;
      }
      else if(lis[i -1] == name && lis[i] == "Movie"){
	std::cout<<lis[i - 1]<<"  "<<lis[i]<<"  "<<lis[i + 1]<<"  "<<lis[i + 2]<<"  "<<lis[i + 3]<<std::endl;
      }
      else if(lis[i -1] == name && lis[i] == "Comedy"){
	std::cout<<lis[i - 1]<<"  "<<lis[i]<<"  "<<lis[i + 1]<<"  "<<lis[i + 2]<<std::endl;
	for(int j = i + 3; j < 200; j++){
	  std::cout<<lis[j]<<" ";
	}
	std::cout<<std::endl;
      }
      if (lis[i] == "Movie"){
	i++;
      }
      
      if (lis[i] == "Comedy"){
	i+=200;
      }
    }
  } else {
    std::cout<<name<<" event is not exist\n";
  }
}

//Changes specific element of a vector<string>.
void add_book(std::vector<std::string> &ev, std::string name, std::string amount){
  int size = ev.size();
  int seat1;
  std::string seat;
  if(check_number(amount)){
    if(std::stoi(amount) > 0){
      if(check_event(ev, name)){
	for (int i = 1; i < size; i+=4){
	  if ((ev[i - 1] == name) && ((ev[i] == "Concert") || (ev[i] == "Movie"))){
	    if ((std::stoi(ev[i + 1]) + std::stoi(amount)) <=std::stoi(ev[i + 2])){
	      ev[i + 1] = std::to_string(std::stoi(ev[i + 1]) + std::stoi(amount));
	      std::cout<<"Successfully booked "<<amount<<" spaces in "<<name<<" event\n";
	    } else {
	      std::cout<<"There's no space for this amount: "<<amount<<" for event "<<name<<" Space left to book: "<<std::to_string(std::stoi(ev[i + 2]) - std::stoi(ev[i + 1]))<<"\nPlease enter less amount\n"; 
	    }
	  }
	  if ((ev[i - 1] == name) && (ev[i] == "Comedy")){
	    if ((std::stoi(ev[i + 1]) + std::stoi(amount)) <=std::stoi(ev[i + 2])){
	      ev[i + 1] = std::to_string(std::stoi(ev[i + 1]) + std::stoi(amount));   
	      for (int j = 0; j < std::stoi(amount); j+=0){
		std::cout<<"Please enter the seat number:\n";
		std::cin>>seat;
		if(check_number(seat)){
		  seat1 = std::stoi(seat);
		  if(seat1 > 0 && seat1 <= 200){
		    if(ev[i + 2 + seat1] != "1"){
		      ev[i + 2 + seat1] = "1";
		      j++;
		    } else {
		      std::cout<<"The seat number: "<<seat<<" is already booked try other seat\n";
		    }
		  } else {
		    std::cout<<"Please enter a number (1 - 200)\n";
		  }
		} else {
		  std::cout<<"Please enter only numbers\n";
		}
	      }
	      std::cout<<"Successfully booked "<<amount<<" spaces in "<<name<<" event\n";
	    } else {
	      std::cout<<"There's no space for this amount: "<<amount<<" for event "<<name<<" Space left to book: "<<std::to_string(std::stoi(ev[i + 2]) - std::stoi(ev[i + 1]))<<"\nPlease enter less amount\n"; 
	    }
	  }
	  if (ev[i] == "Movie"){
	    i++;
	  }
	  if (ev[i] == "Comedy"){
	    i+=200;
	  }
	}
      }else{
	std::cout<<"Event: "<<name<<" is not exist! Please enter a valid name next time\n";
      }
    } else {
      std::cout<<"Please enter positive number for amount next time\n";
    }
  } else {
    std::cout<<"Please enter only numbers for amount next time\n";
  }
}
