CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic

.PHONY : all
all : output

output: main.cpp functions.o event.o
	$(CXX) $(CXXFLAGS) -o $@ $^
functions.o: functions.cpp functions.h
	$(CXX) $(CXXFLAGS) -c $<
event.o: event.cpp event.h
	$(CXX) $(CXXFLAGS) -c $<

all : function_tester

function_tester: function_tester.cpp functions.o
	$(CXX) $(CXXFLAGS) -o $@ $^


.PHONY : clean
clean :
	rm *.o
	rm output.exe
	rm function_tester.exe


